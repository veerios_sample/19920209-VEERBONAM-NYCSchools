//
//  DetailViewController.swift
//  NYCSchools
//
//  Created by Veer on 15/03/23.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet var lblDbn, lblSchool_name, lblNum_of_sat_test_takers, lblSat_critical_reading_avg_score, lblSat_math_avg_score, lblSat_writing_avg_score: UILabel?
    
    var schoolDetails: SchoolDtails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "School Details"
        setUpView()
    }
    
    /*
     I don't have enough time to design proper UI for this screen as i am busy with my current project deliverables.
     */
    func setUpView(){
        lblDbn?.text = "Dbn : " + (schoolDetails?.dbn ?? "")
        lblSchool_name?.text =  "School Name : " + (schoolDetails?.school_name ?? "")
        lblSat_critical_reading_avg_score?.text =  "Sat critical reading avg score : " + (schoolDetails?.sat_critical_reading_avg_score ?? "")
        lblNum_of_sat_test_takers?.text =  "Sat test takers : " + (schoolDetails?.num_of_sat_test_takers ?? "")
        lblSat_math_avg_score?.text =  "Sat math avg score : " + (schoolDetails?.sat_math_avg_score ?? "")
        lblSat_writing_avg_score?.text =  "Sat writing avg score : " + (schoolDetails?.sat_writing_avg_score ?? "")
    }
}
