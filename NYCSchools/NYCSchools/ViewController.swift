//
//  ViewController.swift
//  NYCSchools
//
//  Created by Veer on 15/03/23.
//

import UIKit


struct School: Codable{
    let dbn: String
    let school_name: String
    let phone_number: String
}

struct SchoolDtails: Codable{
    let dbn: String
    let school_name: String
    let num_of_sat_test_takers: String
    let sat_critical_reading_avg_score: String
    let sat_math_avg_score: String
    let sat_writing_avg_score: String
}

enum Api: String {
    case schools = "s3k6-pzi2.json"
    case school_details = "f9bf-2cp4.json"
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var tblSchoolList: UITableView!
    @IBOutlet var viewLoader: UIView?
    @IBOutlet var loadingIndicator: UIActivityIndicatorView?
    
    let cellReuseIdentifier = "CustomCell"
    
    var arraySchoolList = [School]()
    var arrayScoolsDetails = [SchoolDtails]()
    
    var base_url: String? = Bundle.main.object(forInfoDictionaryKey: "base_url") as? String
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblSchoolList.delegate = self
        tblSchoolList.dataSource = self
        navBarSetUp()
        self.showLoader()
        callSchoolListAPI()
        callSchoolDetailsAPI()
    }
    
    // Navigation bar setup
    func navBarSetUp(){
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = .blue
        appearance.titleTextAttributes = [.font: UIFont.boldSystemFont(ofSize: 20.0),
                                          .foregroundColor: UIColor.white]
        
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
    }
    
    // MARK: - UITableView
    // MARK: -
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraySchoolList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CustomCell = self.tblSchoolList.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! CustomCell
        let dic:School = arraySchoolList[indexPath.row]
        cell.lblSchoolName.text = dic.school_name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let detailVC = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController {
            
            let dic: School = arraySchoolList[indexPath.row]
            if let schoolDetails = arrayScoolsDetails.filter({$0.dbn == dic.dbn}).first {
                detailVC.schoolDetails = schoolDetails
            } else {
                detailVC.schoolDetails = SchoolDtails(dbn: dic.dbn, school_name: dic.school_name, num_of_sat_test_takers: "NA", sat_critical_reading_avg_score: "NA", sat_math_avg_score: "NA", sat_writing_avg_score: "NA")
            }
            present(detailVC, animated: true, completion: nil)
        }
    }
    
    // MARK: - API
    // MARK: -
    
    /*
     Given enough time i would have setup CocoaPods and integrate Alamofire.
     */
    func callSchoolListAPI(){
        guard let url = URL(string: (base_url ?? "") + Api.schools.rawValue) else{
            return
        }
        
        let task = URLSession.shared.dataTask(with: url){
            data, response, error in
            
            let decoder = JSONDecoder()
            
            if let data = data{
                do{
                    let schoolData = try decoder.decode([School].self, from: data)
                    self.arraySchoolList = schoolData
                    DispatchQueue.main.async {
                        self.tblSchoolList.reloadData()
                    }
                    self.hideLoader()
                }catch{
                    print(error)
                    self.hideLoader()
                }
            }
        }
        
        task.resume()
    }
    
    
    func callSchoolDetailsAPI(){
        guard let url = URL(string: (base_url ?? "") + Api.school_details.rawValue) else{
            return
        }
        
        let task = URLSession.shared.dataTask(with: url){
            data, response, error in
            
            let decoder = JSONDecoder()
            
            if let data = data{
                do{
                    let schoolData = try decoder.decode([SchoolDtails].self, from: data)
                    self.arrayScoolsDetails = schoolData
                    self.hideLoader()
                }catch{
                    print(error)
                    self.hideLoader()
                }
            }
        }
        task.resume()
    }
    
    
    // MARK: - Loading Indicator
    // MARK: -
    
    /*
     I would have used some other third party Activity indicator via CocoaPods
     */
    
    func showLoader(){
        self.viewLoader?.isHidden = false
        self.loadingIndicator?.startAnimating()
    }
    
    func hideLoader(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.viewLoader?.isHidden = true
            self.loadingIndicator?.stopAnimating()
        }
    }
    
}

